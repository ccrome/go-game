import svgwrite

linewidth = 1.0
box_w = 22.0
box_l = 23.7
star_point_diameter = 4.0

nx = 19
ny = 19

dwg = svgwrite.Drawing('test.svg', profile='tiny')

total_width = (nx-1)*box_w
total_height = (ny-1)*box_l
print total_width
print total_height


stroke=svgwrite.rgb(1, 1, 1, "RGB")
stroke_width = "1mm"
fill = svgwrite.rgb(100, 100, 100, "%")


margins = box_w / 2
offset_w = margins
offset_l = margins

boxpoints = ((offset_w + 0          , offset_l +            0),
             (offset_w + total_width, offset_l +            0),
             (offset_w + total_width, offset_l + total_height),
             (offset_w + 0          , offset_l + total_height),
             (offset_w + 0          , offset_l +            0))
background= ((0           , 0),
             (total_width+offset_w*2,            0),
             (total_width+offset_w*2, total_height + offset_l*2),
             (0                     , total_height + offset_l*2),
             (0                     ,            0))

def mm(points):
    return points
    print points
    result = list()
    for i in points:
        result.append(tuple(["%fmm" % x for x in i]))
    print tuple(result)
    return result


bgfill = svgwrite.rgb(90, 90, 90, "%")
dwg.add(dwg.rect(
    insert=(0, 0),
    size = (mm(total_width+offset_w*2),
            mm(total_height+offset_l*2)),
    stroke_width=0,
    fill=bgfill))

dwg.add(dwg.polygon(boxpoints, stroke=stroke, fill=bgfill, stroke_width=stroke_width))

for row in range(ny-2):
    dwg.add(
        dwg.line(
            start = mm((offset_w+0,           offset_l+(row+1) * box_l)),
            end   = mm((offset_w+total_width, offset_l+(row+1) * box_l)),
            stroke=stroke,
            stroke_width = stroke_width))
for col in range(nx-2):
    dwg.add(
        dwg.line(
            start = mm((offset_w+(col+1)*box_w, offset_l)),
            end   = mm((offset_w+(col+1)*box_w, offset_l+total_height)),
            stroke=stroke,
            stroke_width = stroke_width))
if (nx < 13):
    star_point_offset = 2
else:
    star_point_offset = 3

center_w = total_width / 2
center_l = total_height / 2

dwg.add(
    dwg.circle(
        center=(offset_w + center_w,
                offset_l + center_l),
        r = "%fmm" % (star_point_diameter/2.0),
        stroke = stroke))

dwg.add(
    dwg.circle(
        center=(offset_w + (star_point_offset*box_w),
                offset_l + (star_point_offset*box_l)),
        r = "%fmm" % (star_point_diameter/2.0),
        stroke_width=0,
        stroke = stroke))
dwg.add(
    dwg.circle(
        center=(total_width + offset_w - (star_point_offset*box_w),
                offset_l + (star_point_offset*box_l)),
        r = "%fmm" % (star_point_diameter/2.0),
        stroke_width=0,
        stroke = stroke))

dwg.add(
    dwg.circle(
        center=(offset_w + (star_point_offset*box_w),
                total_height + offset_l - (star_point_offset*box_l)),
        r = "%fmm" % (star_point_diameter/2.0),
        stroke_width=0,
        stroke = stroke))
dwg.add(
    dwg.circle(
        center=(total_width + offset_w - (star_point_offset*box_w),
                total_height + offset_l - (star_point_offset*box_l)),
        r = "%fmm" % (star_point_diameter/2.0),
        stroke_width=0,
        stroke = stroke))

dwg.save()
